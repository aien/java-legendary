> author：编程界的小学生
>
> date：2021/03/29

# 数组/其他

283.移动零：https://leetcode-cn.com/problems/move-zeroes/

11.盛最多水的容器：https://leetcode-cn.com/problems/container-with-most-water/

（no）15.三数之和：https://leetcode-cn.com/problems/3sum/

66.加一：https://leetcode-cn.com/problems/plus-one/

26.删除有序数组中的重复项：https://leetcode-cn.com/problems/remove-duplicates-from-sorted-array/

189.旋转数组：https://leetcode-cn.com/problems/rotate-array/

（no）88.合并两个有序数组：https://leetcode-cn.com/problems/merge-sorted-array/

（no）215.数组中的第K个最大元素：https://leetcode-cn.com/problems/kth-largest-element-in-an-array

（no）718.最长重复子数组：https://leetcode-cn.com/problems/maximum-length-of-repeated-subarray

# 队列/栈

20.有效的括号：https://leetcode-cn.com/problems/valid-parentheses/

155.最小栈：https://leetcode-cn.com/problems/min-stack/

232.用栈实现队列：https://leetcode-cn.com/problems/implement-queue-using-stacks

225.用队列实现栈：https://leetcode-cn.com/problems/implement-stack-using-queues

# 哈希表

1.两数之和：https://leetcode-cn.com/problems/two-sum/description/

242.有效的字母异位词：https://leetcode-cn.com/problems/valid-anagram/

49.字母异位词分组：https://leetcode-cn.com/problems/group-anagrams/

# 链表

21.合并两个有序链表：https://leetcode-cn.com/problems/merge-two-sorted-lists/

206.反转链表：https://leetcode.com/problems/reverse-linked-list/

24.两两交换链表中的节点：https://leetcode-cn.com/problems/swap-nodes-in-pairs/

141.环形链表：https://leetcode-cn.com/problems/linked-list-cycle/

142.环形链表II：https://leetcode-cn.com/problems/linked-list-cycle-ii/

面试题 02.02.返回倒数第 k 个节点：https://leetcode-cn.com/problems/kth-node-from-end-of-list-lcci/

剑指 Offer 22. 链表中倒数第k个节点：https://leetcode-cn.com/problems/lian-biao-zhong-dao-shu-di-kge-jie-dian-lcof/

面试题 02.03. 删除中间节点：https://leetcode-cn.com/problems/delete-middle-node-lcci/

109.有序链表转换二叉搜索树：https://leetcode-cn.com/problems/convert-sorted-list-to-binary-search-tree/

82.删除排序链表中的重复元素 II：https://leetcode-cn.com/problems/remove-duplicates-from-sorted-list-ii/

237.删除链表中的节点：https://leetcode-cn.com/problems/delete-node-in-a-linked-list/

234.回文链表：https://leetcode-cn.com/problems/palindrome-linked-list/

面试题 02.01.移除重复节点：https://leetcode-cn.com/problems/remove-duplicate-node-lcci/

剑指 Offer 06.从尾到头打印链表：https://leetcode-cn.com/problems/cong-wei-dao-tou-da-yin-lian-biao-lcof/

25.K个一组翻转链表：https://leetcode-cn.com/problems/reverse-nodes-in-k-group

160.相交链表：https://leetcode-cn.com/problems/intersection-of-two-linked-lists

# 树/递归

144.二叉树的前序遍历：https://leetcode-cn.com/problems/binary-tree-preorder-traversal/

94.二叉树的中序遍历：https://leetcode-cn.com/problems/binary-tree-inorder-traversal/

590.N 叉树的后序遍历：https://leetcode-cn.com/problems/n-ary-tree-postorder-traversal/

589.N 叉树的前序遍历：https://leetcode-cn.com/problems/n-ary-tree-preorder-traversal/

230.二叉搜索树中第K小的元素：https://leetcode-cn.com/problems/kth-smallest-element-in-a-bst/

226.翻转二叉树：https://leetcode-cn.com/problems/invert-binary-tree/

100.相同的树：https://leetcode-cn.com/problems/same-tree/

617.合并二叉树：https://leetcode-cn.com/problems/merge-two-binary-trees/

（no）98.验证二叉搜索树：https://leetcode-cn.com/problems/validate-binary-search-tree/

（no）108.将有序数组转换为二叉搜索树：https://leetcode-cn.com/problems/convert-sorted-array-to-binary-search-tree/

（no）109.有序链表转换二叉搜索树：https://leetcode-cn.com/problems/convert-sorted-list-to-binary-search-tree/

（no）236.二叉树的最近公共祖先：https://leetcode-cn.com/problems/lowest-common-ancestor-of-a-binary-tree/

（no）105.从前序与中序遍历序列构造二叉树：https://leetcode-cn.com/problems/construct-binary-tree-from-preorder-and-inorder-traversal/

# 深度优先/广度优先

深度优先模板：[https://shimo.im/docs/ddgwCccJQKxkrcTq](https://shimo.im/docs/ddgwCccJQKxkrcTq)

广度优先模板：https://shimo.im/docs/P8TqKHGKt3ytkYYd/read

（no）22.括号生成：https://leetcode-cn.com/problems/generate-parentheses/

102.二叉树的层序遍历：https://leetcode-cn.com/problems/binary-tree-level-order-traversal/

429.N 叉树的层序遍历：https://leetcode-cn.com/problems/n-ary-tree-level-order-traversal/

515.在每个树行中找最大值：https://leetcode-cn.com/problems/find-largest-value-in-each-tree-row/

104.二叉树的最大深度：https://leetcode-cn.com/problems/maximum-depth-of-binary-tree/

559.N叉树的最大深度：https://leetcode-cn.com/problems/maximum-depth-of-n-ary-tree/

（no）111.二叉树的最小深度：https://leetcode-cn.com/problems/minimum-depth-of-binary-tree/

# 分治/回溯 

分治模板：[https://shimo.im/docs/3xvghYh3JJPKwdvt](https://shimo.im/docs/3xvghYh3JJPKwdvt)

50.Pow(x, n)：https://leetcode-cn.com/problems/powx-n/solution/

78.子集：https://leetcode-cn.com/problems/subsets/

46.全排列：https://leetcode-cn.com/problems/permutations/

17.电话号码的字母组合：https://leetcode-cn.com/problems/letter-combinations-of-a-phone-number/

169.多数元素：https://leetcode-cn.com/problems/majority-element/

77.组合：https://leetcode-cn.com/problems/combinations/

51.n皇后：https://leetcode-cn.com/problems/n-queens/

# 贪心算法

435.无重叠区间：https://leetcode-cn.com/problems/non-overlapping-intervals/

56.合并区间：https://leetcode-cn.com/problems/merge-intervals/

121.买卖股票的最佳时机：https://leetcode-cn.com/problems/best-time-to-buy-and-sell-stock/

# 二分法

模板：https://shimo.im/docs/hjQqRQkGgwd9g36J/read

69.x 的平方根：https://leetcode-cn.com/problems/sqrtx/

70.有效的完全平方数：https://leetcode-cn.com/problems/valid-perfect-square/

34.在排序数组中查找元素的第一个和最后一个位置：https://leetcode-cn.com/problems/find-first-and-last-position-of-element-in-sorted-array/

33.搜索旋转排序数组：https://leetcode-cn.com/problems/search-in-rotated-sorted-array/

153.寻找旋转排序数组中的最小值：https://leetcode-cn.com/problems/find-minimum-in-rotated-sorted-array/

# 位运算

191.位1的个数：https://leetcode-cn.com/problems/number-of-1-bits/

231.2的幂：https://leetcode-cn.com/problems/power-of-two/

190.颠倒二进制位：https://leetcode-cn.com/problems/reverse-bits/

338.比特位计数：https://leetcode-cn.com/problems/counting-bits/description/

# LRU

146.LRU缓存机制：https://leetcode-cn.com/problems/lru-cache/#/

# 字典树/红黑树/AVL树/并查集

并查集模板：https://shimo.im/docs/ydPCH33xDhK9YwWR/read

208.实现 Trie (前缀树)：https://leetcode-cn.com/problems/implement-trie-prefix-tree/

200.岛屿数量：https://leetcode-cn.com/problems/number-of-islands/

36.有效的数独：https://leetcode-cn.com/problems/valid-sudoku/description/

AVL：为什么要有？左旋、右旋、左右旋、右左旋

110.平衡二叉树：https://leetcode-cn.com/problems/balanced-binary-tree

# 排序

1.十大经典排序算法：https://www.cnblogs.com/onepixel/p/7674659.html

2.快排：https://shimo.im/docs/98KjvGwwGpTpYGKy/

3.堆排：https://shimo.im/docs/6kRVHRphpgjHgCtx/

9种经典排序算法可视化动画：https://www.bilibili.com/video/av25136272

6分钟看完 15 种排序算法动画展示：https://www.bilibili.com/video/av63851336

1122.数组的相对排序：https://leetcode-cn.com/problems/relative-sort-array/

# 字符串

大整数相加. 正常相加会溢出的两个整数, 以字符串方式相加

统计数组中每个字符串出现次数

未完待续

# 动态规划

70.爬楼梯：https://leetcode-cn.com/problems/climbing-stairs/

未完待续