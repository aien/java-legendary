[TOC]

> author：编程界的小学生
>
> date：2021/03/03

# 一、SpringMVC的流程

具体步骤：

- 第一步：发起请求到前端控制器(DispatcherServlet) 

- 第二步：前端控制器请求HandlerMapping查找 Handler （可以根据xml配置、注解进行查找） 

- 第三步：处理器映射器HandlerMapping根据请求url找到具体的处理器Handler返回给DispatcherServlet；HandlerMapping会把请求映射为HandlerExecutionChain对象（包含一个Handler页面处理器对象和多个HandlerInterceptor拦截器对象），通过这种策略模式，很容易添加新的映射策略 

- 第四步：前端控制器调用HandlerAdapter处理器适配器去执行Handler 

- 第五步：处理器适配器HandlerAdapter将会根据适配的结果去执行Handler （具体的业务逻辑）

- 第六步：Handler执行完成给适配器返回ModelAndView 

- 第七步：处理器适配器向前端控制器返回执行结果ModelAndView （ModelAndView是springmvc框架的一个底层对象，包括 Model和view） 

- 第八步：DispatcherServlet将ModelAndView传给ViewResolver视图解析器进行解析；

- 第九步：视图解析器ViewResolver向前端控制器返回View 

- 第十步：DispatcherServlet对View进行视图渲染 （视图渲染将模型数据(在ModelAndView对象中)填充到request域） 

- 第十一步：DispatcherServlet向用户响应结果

![springmvc-1](images/springmvc-1.png)